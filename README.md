# Welcome to my first CMD game named Rock-Paper-Scissors.

### Info:
This is Rock-Paper-Scissors but in commandprompt, the "cmdFistGame" directory contains the sourcecode. 
And "This is the game" directory contains the zip file(download it, unzip it and double click the bat file). 


### How to play:

If your not familiar with the game: "A player who decides to play rock will beat another player who has chosen scissors 
("rock crushes scissors"), but will lose to one who has played paper ("paper covers rock");
a play of paper will lose to a play of scissors ("scissors cuts paper").
If both players choose the same shape, the game is tied and is usually immediately replayed to break the tie." [Click here for more info](https://en.wikipedia.org/wiki/Rock–paper–scissors)


### Commands:

In game there are four commands: 
- end(to terminate the game) 
- scissors(to pick scissors)  
- rock(to pick rock) 
- paper(to pick paper)

### Current game mechanics:
After each draw, win, loss the game continues the only way to stop it is by typing "end".



## Created by VerdommeMan.
