package cmdFistGame;

import java.util.*;

public class Fist {
	private boolean game = true;
	private String cmdInput;

	public static void main(String[] args) {
	     
		System.out.println("This is  a Rock-Paper-Scissors game in CMD.\n\nYou can use the following commands: end, scissors, paper, rock"+ ".\n\nThe game started:");
		Scanner s = new Scanner(System.in);
		Fist f = new Fist();
		
		while(f.isGame()) {
			f.setCmdInput(s.nextLine());
			if(f.getCmdInput().equals("end")) {
				f.setGame(false);
				
			}else if(f.getCmdInput().equals("scissors") ||f.getCmdInput().equals("rock")|| f.getCmdInput().equals("paper") ) {
				System.out.printf("You picked: %s.\n", f.getCmdInput());
				f.winner();
				
			}else{
				System.out.printf("I could not read that: \"%s\"\n", f.getCmdInput());
				
			}
		}
		
		s.close();
		System.out.println("The game is terminated.");
		System.out.println("♫♫♫♫♫ Made by VerdommeMan ♫♫♫♫♫");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		
	}
	
	//It chooses who the winner is.
	public void winner() {
		String p2 =player2();
		System.out.printf("Bot picked: %s.\n", p2);
		
		if(cmdInput.equals(p2)){
			System.out.println("\n#######--Draw--#######");
		}
		else if(cmdInput.equals("scissors") && p2.equals("paper")) {
			System.out.println("\n#######--Player wins!--#######");
		}
		else if(cmdInput.equals("paper") && p2.equals("scissors")) {
			System.out.println("\n#######--Bot wins!--#######");
		}
		else if(cmdInput.equals("scissors") & p2.equals("rock")) {
			System.out.println("\n#######--Bot wins!--#######");
		}
		else if(cmdInput.equals("rock") & p2.equals("scissors")) {
			System.out.println("\n#######--Player wins!--#######");
		}
		else if(cmdInput.equals("paper") & p2.equals("rock")) {
			System.out.println("\n#######--Player wins!--#######");
		}
		else if(cmdInput.equals("rock") & p2.equals("paper")) {
			System.out.println("\n#######--Bot wins!--#######");
		}
	}
	
	//bot returns his pick
	public String player2() {
		String pick;
		Random r = new Random();
		int n = r.nextInt(3);
		
		switch(n) {
		case 0: pick = "rock"; break;
		case 1: pick = "scissors"; break;
		case 2: pick = "paper"; break;
		default:pick = "paper";
		}
		
		return pick;
	}
	
	//here starts the s-/getters
	public boolean isGame() {
		return game;
	}

	public void setGame(boolean game) {
		this.game = game;
	}

	public String getCmdInput() {
		return cmdInput;
	}

	public void setCmdInput(String cmdInput) {
		this.cmdInput = cmdInput;
	}
	
}
